<h1>Project name here</h1>
<h3>Name of student</h3>
<h3>Studentid</h3>


<h1>Analysis</h1>

<h2>Case description</h2> 
(one-(two) pages of text)
<ul>
<li>actors of the solution</li>
<li>roles of actors</li>
<li>tasks the actors would like to do</li>
<li>Data input</li>
<li>Output data type</li>
<li>What kind of algorithms there are</li>
<h2>Table of functional requirements</h2>
<h2>Project schedule</h2>
<h2>Project plan document (including the schedule) using the template</h2>
<h2>Use case diagram drawn with Visual Paradigm</h2>
![my image](pics/uc.png?raw=true "Use case")
<h2>Test plan</h2>
List 5 test cases you will test

<h1>Design</h1>

<h2>Sequence diagrams (one from client side one from server side)  drawn with Visual Paradigm</h2>
![my image](pics/seq.png?raw=true "Sequence")
<h2>Package diagram drawn with Visual Paradigm</h2>
![my image](pics/pack.png?raw=true "Package")
<h2>Class diagrams (one from client side one from server side)  drawn with Visual Paradigm</h2>
![my image](pics/cl.png?raw=true "Class")
<h2>Two user stories</h2>
Like "As a Finnish citizen I want to know the actual Corona numbers on daily based listing the day, daily new sickness and daily died. I would like to see 30 rows per time and able to scroll next 30."

<h1>Implementation</h1>

<h2>Java Spring Boot back end having minimum two tables</h2>
<h2>React front end having minimum two tables fetching the database data</h2>

<h1>Testing</h1>

<h2>Spring Boot test cases</h2>
<ul>
<li>Add item to database
<li>Get one from database based on id
<li>Get all from database and compare amount
<li>Test RESTful API GET one
<li>Test RESTful API POST 
</ul>
<h2>React Test cases</h2>
Test that the table (or tables) exist

<h1>Documentation</h1>
<ul>
<li>Swagger documentation for RESTful API</li>
<li>README.md documentation for Spring Boot backend</li>
<li>README.md documentation for React front end (different gitlab -project)</li>
</ul>
<h1>Deployment</h1>
<ul>
<li>Back end deployed to Heroku</li>
<li>Front end either in http://www.cc.puv.fi/~e1700000/final or in Heroku</li>
</ul>